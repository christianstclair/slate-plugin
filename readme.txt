=== Slate Admin Theme ===
Contributors: christian st. clair
Donate Link: 
Tags: wordpress admin theme, admin theme, white label, admin page, wordpress admin panel, admin, plugin, admin panel, wordpress, wordpress admin panel, flat admin theme, modern admin theme, simple admin theme, admin theme style plugin, free admin theme style plugin, backend theme, custom admin theme, new admin ui, wp admin theme, wp admin page.
Requires at least: 4.0
Tested up to: 4.7
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Plugin to coincide with the Slate Theme from Foundations Recovery Network.

== Description ==


== Installation ==

1. Unzip `slate-admin.zip`
2. Upload the `slate-admin' folder to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

1.0.3
- Styles: changed scss styles to look more like wordpress.
- Admin Bar: changed home icon to link to a site home page and open in a new tab.

1.0.1
* Styles: Changes were made
* Admin Bar: Changed Wordpress Logo to Site Title

1.0.0
* initializing of plugin
* changed scss and divided up partials

== Upgrade Notice ==
