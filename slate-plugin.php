<?php

/*
Plugin Name: Slate Plugin
Plugin URI: https://bitbucket.org/christianstclair/slate-plugin/get/master.zip
Description: A WordPress Admin theme
Author: christianstclair
Version: 1.0.3
Author URI: http://christianstclair.com/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/christianstclair/slate-plugin',
    __FILE__,
    'slate-plugin'
);

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');


function slate_files() {
	wp_enqueue_style( 'slate-admin-theme', plugins_url('css/slate.css', __FILE__), array( 'acf-input' ), '1.1.7' );
	wp_enqueue_script( 'slate', plugins_url( "js/slate.js", __FILE__ ), array( 'jquery' ), '1.1.7' );

	$pagetemplate = array( 'page_template' => basename(get_page_template()) );
	wp_localize_script( 'slate', 'template', $pagetemplate );

}

// Add a custom Site Title to the Dashboard Toolbar
function my_admin_bar() {
	global $wp_admin_bar;
	
	$args = array(
        'id'     => 'wp-logo',                         // id of the existing child node (New > Post)
        'title'  => __( get_bloginfo('name'), 'textdomain' ), // alter the title of existing node
        'parent' => false,                              // set parent to false to make it a top level (parent) node
    );

    $sitename = array(
        'id'     => 'site-name',                         // id of the existing child node (New > Post)
        'title'  => __( get_bloginfo('name'), 'textdomain' ), // alter the title of existing node
        'parent' => false,                              // set parent to false to make it a top level (parent) node
        'meta' => array( 'target' => site_url() ), // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
    );

	//Add a link called 'My Link'...
	$wp_admin_bar->add_node($args);
	$wp_admin_bar->add_node($sitename);
	$wp_admin_bar->remove_menu('view-site');

}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar', 999 ); 


/**
 * Hide editor on specific pages.
 *
 */

function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;
  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if($template_file == 'template-home.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
}
add_action( 'admin_head', 'hide_editor' );

add_action( 'admin_enqueue_scripts', 'slate_files' );
add_action( 'login_enqueue_scripts', 'slate_files' );


function slate_add_editor_styles() {
    add_editor_style( plugins_url('css/editor-style.css', __FILE__ ) );
}
add_action( 'after_setup_theme', 'slate_add_editor_styles' );
add_filter('admin_footer_text', 'slate_admin_footer_text_output');


function slate_admin_footer_text_output($text) {
	$text = 'WordPress Admin Theme by christian st. clair';
  return $text;
}

add_action( 'admin_head', 'slate_colors' );
add_action( 'login_head', 'slate_colors' );
function slate_colors() {
	include( 'css/dynamic.php' );
}
function slate_get_user_admin_color(){
	$user_id = get_current_user_id();
	$user_info = get_userdata($user_id);
	if ( !( $user_info instanceof WP_User ) ) {
		return; 
	}
	$user_admin_color = $user_info->admin_color;
	return $user_admin_color;
}

// Remove the hyphen before the post state
add_filter( 'display_post_states', 'slate_post_state' );
function slate_post_state( $post_states ) {
	if ( !empty($post_states) ) {
		$state_count = count($post_states);
		$i = 0;
		foreach ( $post_states as $state ) {
			++$i;
			( $i == $state_count ) ? $sep = '' : $sep = '';
			echo "<span class='post-state'>$state$sep</span>";
		}
	}
}

?>